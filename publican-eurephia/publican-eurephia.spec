%define brand eurephia

Name:		publican-eurephia
Summary:	Common documentation files for %{brand}
Version:	0.3
Release:	0%{?dist}
License:	CC-BY-SA
Group:		Applications/Text
Buildroot:	%{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
Buildarch:	noarch
Source:		http://www.eurephia./net/documentation/source/%{name}-%{version}.tgz
Requires:	publican >= 1.0
BuildRequires:	publican >= 1.0
URL:		http://www.eurephia.net/

%description
This package provides common files and templates needed to build documentation
for %{brand} with publican.

%prep
%setup -q 

%build
publican build --formats=xml --langs=all --publish

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p -m755 $RPM_BUILD_ROOT%{_datadir}/publican/Common_Content
publican install_brand --path=$RPM_BUILD_ROOT%{_datadir}/publican/Common_Content

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc README
%doc COPYING
%{_datadir}/publican/Common_Content/%{brand}

%changelog
* Thu May 20 2010 David Sommerseth <dazo@users.sourceforge.net> - 0.2-0
- Added trademark legal notice for Red Hat brands

* Sun May 16 2010  David Sommerseth <dazo@users.sourceforge.net> 0.1
- Created Brand

